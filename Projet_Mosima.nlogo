;variable global utilisé pour le calcul de la moyenne et le graphique
globals[ moyenne  lmoyenne time plot-color]

;agents variables
patches-own[
  orientation
  actif?
  agent_type
  agent_type_color
  effort_color
  effort
  profit
  
  num_interaction
  
  leffort
  cumeffort
  lprofit
  aeffort
  aprofit
  
  bonus ; variable introduite pour la question 3.5
  
  debug ; juste utilisé lors du debugage
]

;addition to the paper model, leaders are turtles they will be able to move freely and interact with a larger number of agents
breed[leaders leader]

;learders variable
leaders-own[
  
]

;create a leader with the button, possible in any kind of configuration
to add_leader
  create-leaders 1
  [
    setxy random-xcor random-ycor
  ]
  
end

to setup_random
  clear-all
  
  ask patches[set actif? False]
  
  ask n-of 50 patches with [actif? = False]
  [
    set actif? True
    set agent_type "high effort"
  ]
  
  ask n-of 400 patches with [actif? = False]
  [
    set actif? True
    set agent_type "shrinking effort"
  ]
  
  
  init_agent
  
  refresh_color
    
  reset-ticks
end

;will difine the colors of any kind of agent and initialise their variables
; this function have to be called in any configuration
to init_agent
  set time 0
  plot-pen-down
  ask patches with [actif? = True]
  [
    set orientation 0
    set effort effort_initial
    set profit 0
    set num_interaction 0
    set leffort 0
    set cumeffort 0
    set lprofit 0
    set aeffort 0
    set aprofit 0
    
    
    
    if agent_type = "null effort"[
      set agent_type_color red
      
    ]
    if agent_type = "shrinking effort"[
      set agent_type_color orange
      
    ]
    if agent_type = "replicator"[
      set agent_type_color yellow
    ]
    if agent_type = "rational"[
      set agent_type_color green
      
    ]
    if agent_type = "profit comparator"[
      set agent_type_color turquoise
      
      
    ]
    if agent_type = "high effort"[
      set agent_type_color cyan
      set effort 2
    ]
    if agent_type = "fixed effort"[
      set agent_type_color cyan + 3
      set effort 1
    ]
    if agent_type = "average rational"[
      set agent_type_color violet
      
    ]
    if agent_type = "winner imitator"[
      set agent_type_color gray
      set effort 2
      ;set aeffort 2
    ]
    if agent_type = "effort comparator"[
      set agent_type_color magenta 
      
    ]
    if agent_type = "averager"[
      set agent_type_color pink
      
    ]
    
    
  ]
  
end

;function use to create all the configurations for the question 3.3 , they are all described in the main interface
; its using the %_high_effort variable
to setup_%higheffort
  clear-all
  
  ask patches[set actif? False]
  
  let num_agent count patches
  
  let num_high_effort int ( (%_high_effort / 100) * num_agent)
  
  let num_other_agent num_agent - num_high_effort
  
  if config_% = 1[
    ask n-of num_high_effort patches with [actif? = False]
    [
      set actif? True
      set agent_type "high effort"
    ]
    
    ask patches with [actif? = False]
    [
      set actif? True
      set agent_type "null effort"
    ]
  ]
  if config_% = 2[
    ask n-of num_high_effort patches with [actif? = False]
    [
      set actif? True
      set agent_type "high effort"
    ]
    
    ask patches with [actif? = False]
    [
      set actif? True
      set agent_type "shrinking effort"
    ]
  ]
  if config_% = 3[
    ask n-of num_high_effort patches with [actif? = False]
    [
      set actif? True
      set agent_type "high effort"
    ]
    
    ask patches with [actif? = False]
    [
      set actif? True
      set agent_type "replicator"
    ]
  ]
  if config_% = 4[
    ask n-of num_high_effort patches with [actif? = False]
    [
      set actif? True
      set agent_type "high effort"
    ]
    
    ask patches with [actif? = False]
    [
      set actif? True
      set agent_type "profit comparator"
    ]
  ]
  if config_% = 5[
    ask n-of num_high_effort patches with [actif? = False]
    [
      set actif? True
      set agent_type "high effort"
    ]
    
    ask patches with [actif? = False]
    [
      set actif? True
      set agent_type "winner imitator"
    ]
  ]
  if config_% = 6[
    ask n-of num_high_effort patches with [actif? = False]
    [
      set actif? True
      set agent_type "high effort"
    ]
    
    ask patches with [actif? = False]
    [
      set actif? True
      set agent_type "effort comparator"
    ]
  ]
  if config_% = 7[
    ask n-of num_high_effort patches with [actif? = False]
    [
      set actif? True
      set agent_type "high effort"
    ]
    
    ask patches with [actif? = False]
    [
      set actif? True
      set agent_type "averager"
    ]
  ]
  if config_% = 8[
    ask n-of num_high_effort patches with [actif? = False]
    [
      set actif? True
      set agent_type "high effort"
    ]
    
    ask patches with [actif? = False]
    [
      set actif? True
      set agent_type "rational"
    ]
  ]
  if config_% = 9[
    ask n-of num_high_effort patches with [actif? = False]
    [
      set actif? True
      set agent_type "high effort"
    ]
    
    ask patches with [actif? = False]
    [
      set actif? True
      set agent_type "average rational"
    ]
  ]
  
  
  
  init_agent
  
  refresh_color
  
  reset-ticks

end

;setup all the configurations for the question 3.1   and  3.2  and  3.4
to setup_i
  clear-all ; for the question 3.4 we dont use clear-all to keep all plot drawing
  
  ask patches[set actif? False]
  
  if config_i = 1[
    ask patches with [actif? = False]
    [
      set actif? True
      set agent_type "rational"
    ]
  ]
  
  if config_i = 2[
    ask patches with [actif? = False]
    [
      set actif? True
      set agent_type "shrinking effort"
    ]
  ]
  
  if config_i = 3[
    ask one-of patches with [actif? = False]
    [
      set actif? True
      set agent_type "fixed effort"
    ]
    ask patches with [actif? = False]
    [
      set actif? True
      set agent_type "replicator"
    ]
  ]
  
  if config_i = 4[
    ask one-of patches with [actif? = False]
    [
      set actif? True
      set agent_type "rational"
    ]
    ask patches with [actif? = False]
    [
      set actif? True
      set agent_type "replicator"
    ]
  ]
  if config_i = 5[
    ask one-of patches with [actif? = False]
    [
      set actif? True
      set agent_type "high effort"
    ]
    ask patches with [actif? = False]
    [
      set actif? True
      set agent_type "replicator"
    ]
  ]
  if config_i = 6[
    ask n-of 40 patches with [actif? = False]
    [
      set actif? True
      set agent_type "high effort"
    ]
    ask patches with [actif? = False]
    [
      set actif? True
      set agent_type "effort comparator"
    ]
  ]
  if config_i = 7[
    ask n-of 40 patches with [actif? = False]
    [
      set actif? True
      set agent_type "high effort"
    ]
    ask n-of 40 patches with [actif? = False]
    [
      set actif? True
      set agent_type "null effort"
    ]
    ask patches with [actif? = False]
    [
      set actif? True
      set agent_type "replicator"
    ]
  ]
  if config_i = 10[
    ask patches with [actif? = False]
    [
      set actif? True
      set agent_type "winner imitator"
    ]
  ]
  
  init_agent
  
  refresh_color
  
  reset-ticks
end


; main function used for all configuration
to go
  set time time + 1 ; variable used for ploting
  refresh_color ;refresh what kind of "affichage" we want to see   ( agent type or agent effort)
  
  ;first we move each agent
  ask patches with [actif? = True][  
    move_randomly
  ]
  ;then we make them interact
  ask patches with [actif? = True][
    interact
  ]
  ;and here we will update all information (profit effort etc)
  ask patches with [actif? = True][
    calcul_profit
  ]
  
  ; this part is for question 3.5
  ;there is 2 kind of interaction (one is curently in comentary
  ask leaders[
    ;move the leader agent
    rt random 50
    lt random 50
    fd 0.5
    
    ;make it interact
    ask patches with [ distance myself < 1 ]
    [ ;first case
      ;set effort effort + 0.01  
      
      ;2nd case
      let truc agent_type   
      
      
      let somme 0
      ask patches with [actif? = True and agent_type = truc ]
      [
        set somme somme + effort
      ]
      
      let num_other count patches with [ actif? = True and agent_type = truc ]
      
      
      ifelse effort > somme / num_other [
        set bonus 2
      ]
      [set bonus -2]
    ]
    
  ]
  
  let somme 0
  ask patches with [actif? = True]
  [
    set somme somme + effort
  ]
  let compteur count patches with [actif? = True]
  set moyenne somme / compteur
  if abs( moyenne - lmoyenne) < 0.0001
  [
    show moyenne
  ]
  set lmoyenne moyenne
  tick
end

; function to move an agent (patch)
to  move_randomly
  
  ;what direction?
  set orientation random 5
  let x_futur_patch 0
  let y_futur_patch 0
  if orientation = 1[
    set x_futur_patch pxcor
    set y_futur_patch pycor + 1
  ]
  
  if orientation = 2[
    set x_futur_patch pxcor + 1
    set y_futur_patch pycor
  ]
  
  if orientation = 3[
    set x_futur_patch pxcor
    set y_futur_patch pycor - 1
  ]
  
  if orientation = 4[
    set x_futur_patch pxcor - 1
    set y_futur_patch pycor
  ]
  if orientation = 0[
    set x_futur_patch pxcor
    set y_futur_patch pycor
  ]
  
  ; we ask the patch in the move direction if it is curently use or if its free
  let tampon_actif? False
  ask patch x_futur_patch y_futur_patch[set tampon_actif? actif?]

  ;if its free we move
  if tampon_actif? = False[
    ;;on remplace les valeur et reset l'actuel patch
    ;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ;mettre à jour lors de lajout d'atribut au agent
    let tampon_orientation orientation
    let tampon_couleur pcolor
    let tampon_agent_type_color agent_type_color
    let tampon_agent_type agent_type
    
    let tampon_effort effort
    let tampon_leffort leffort
    let tampon_aeffort aeffort
    
    let tampon_profit profit
    let tampon_aprofit aprofit
    let tampon_lprofit lprofit
    
    let tampon_num_interaction num_interaction
    let tampon_cumeffort cumeffort
    
    ;paste all value in the new patch
    ask patch x_futur_patch y_futur_patch[
      set orientation tampon_orientation
      set pcolor tampon_couleur
      set actif? True
      set agent_type_color tampon_agent_type_color
      set agent_type tampon_agent_type
      
      set effort tampon_effort 
      set leffort tampon_leffort
      set aeffort tampon_aeffort
      
      set profit tampon_profit
      set lprofit tampon_lprofit
      set aprofit tampon_aprofit
      
      set num_interaction tampon_num_interaction
      set cumeffort tampon_cumeffort
    ]
    
    ;put the curent patch nul
    set agent_type_color 0
    set orientation 0
    set pcolor 0
    set actif? False
    set effort 0
    set agent_type 0
    set orientation 0
    set profit 0
    set num_interaction 0
    set leffort 0
    set cumeffort 0
    set lprofit 0
    set aeffort 0
    set aprofit 0
  ]
  
  ;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end

;function for the interaction
to interact
  let orientation_potential_coop_partener 0
  let x_potential_coop_partener 0
  let y_potential_coop_partener 0
  if orientation = 0[
    
  ]
  if orientation = 1[
    set orientation_potential_coop_partener 3
    set x_potential_coop_partener pxcor
    set y_potential_coop_partener pycor + 1
  ]
  if orientation = 2[
    set orientation_potential_coop_partener 4
    set x_potential_coop_partener pxcor + 1
    set y_potential_coop_partener pycor
  ]
  if orientation = 3[
    set orientation_potential_coop_partener 1
    set x_potential_coop_partener pxcor
    set y_potential_coop_partener pycor - 1
  ]
  if orientation = 4[
    set orientation_potential_coop_partener 2
    set x_potential_coop_partener pxcor - 1
    set y_potential_coop_partener pycor
  ]
  let bool_partener_found False
  
  ;we ask the possible patch partener if it have the good orientation
  ask patch x_potential_coop_partener y_potential_coop_partener[
    if orientation = orientation_potential_coop_partener and actif? = True[
      set bool_partener_found True
    ]
  ]
  
  ; if it have the good orientation then
  if bool_partener_found[
    set num_interaction num_interaction + 1
    
       
    
    ;transfert effort to partener
    let tampon_effort effort
    ask patch x_potential_coop_partener y_potential_coop_partener
    [
      ;noise value
      let noise_val 1 - noise + ( 2 * random-float noise)
      set aeffort tampon_effort * noise_val
      
    ]
    

    set leffort effort
    
  ]
  
  
end

;update profit and effort value after interacting
to calcul_profit
  let orientation_potential_coop_partener 10
  let x_potential_coop_partener 0
  let y_potential_coop_partener 0
  if orientation = 0[
    
  ]
  if orientation = 1[
    set orientation_potential_coop_partener 3
    set x_potential_coop_partener pxcor
    set y_potential_coop_partener pycor + 1
  ]
  if orientation = 2[
    set orientation_potential_coop_partener 4
    set x_potential_coop_partener pxcor + 1
    set y_potential_coop_partener pycor
  ]
  if orientation = 3[
    set orientation_potential_coop_partener 1
    set x_potential_coop_partener pxcor
    set y_potential_coop_partener pycor - 1
  ]
  if orientation = 4[
    set orientation_potential_coop_partener 2
    set x_potential_coop_partener pxcor - 1
    set y_potential_coop_partener pycor
  ]
  
  ;ask the possible partener if it have the good orientation
  let bool_partener_found False
  ask patch x_potential_coop_partener y_potential_coop_partener[
    if orientation = orientation_potential_coop_partener and actif? = True
    [
      set bool_partener_found True
    ]
  ]
  
  ;if he do then 
  if bool_partener_found[
         
        
    set cumeffort cumeffort + aeffort
    
    set lprofit profit
    set profit 5 * sqrt(leffort + aeffort) - (leffort * leffort) + bonus
    set aprofit 5 * sqrt(aeffort + leffort) - (aeffort * aeffort)   
    
    ;after using the bonus we put it at 0 
    set bonus 0
    
    ;update all effort value coresponding to the agent type
    if agent_type = "null effort"[
      set effort 0.0001
      
    ]
    if agent_type = "shrinking effort"[
      set effort aeffort / 2
      
    ]
    if agent_type = "replicator"[
      set effort aeffort
      
    ]
    if agent_type = "rational"[
      let ej aeffort
      let p (- 1 * ej * ej) / 3
      let q ((2 * ej * ej * ej) / 27) - (25 / 16)
      let discreminant  (- 4 * p * p * p - 27 * q * q)
      if discreminant <= 0[
        let u ( 0.5 * ( - q + sqrt ( (- discreminant) / 27)))^(1 / 3)
        let v ( 0.5 * ( - q - sqrt ( (- discreminant) / 27)))^(1 / 3)
        let z u + v
        set effort z - (ej / 3)
      ]
    ]
    if agent_type = "profit comparator"[
      ;augmente de 10% si le profit augmente
      
      ifelse profit > aprofit
      [
        set effort leffort * 1.1
      ]
      [
        set effort leffort * 0.9
      ]
      
    ]
    if agent_type = "high effort"[
      set effort 2
    ]
    if agent_type = "fixed effort"[
      set effort effort_agent_fixe
      
    ]
    if agent_type = "average rational"[
      let ej cumeffort / num_interaction
      let p (- 1 * ej * ej) / 3
      let q ((2 * ej * ej * ej) / 27) - (25 / 16)
      let discreminant  (- 4 * p * p * p - 27 * q * q)
      if discreminant <= 0[
        let u ( 0.5 * ( - q + sqrt ( (- discreminant) / 27)))^(1 / 3)
        let v ( 0.5 * ( - q - sqrt ( (- discreminant) / 27)))^(1 / 3)
        let z u + v
        set effort z - (ej / 3)
      ]
    ]
    if agent_type = "winner imitator"[
      if profit < aprofit; si son partner à une profit plus grande
        [set effort aeffort]  ; il va prendre l’effort de son partner
     
    ]
    if agent_type = "effort comparator"[
       ifelse leffort > aeffort
       [set effort leffort * 0.9]
       [set effort leffort * 1.1]
    ]
    if agent_type = "averager"[
      set effort (leffort + aeffort ) / 2
    ]
      
    if effort > 2 [
      set effort 2
    ]
    if effort < 0.0001[
      set effort 0.0001
    ]
    
   
  ]
  
  
end

;change the color coresponding to the "affichage" that we want (agent type or agent effort
to refresh_color
  
  ask patches with [actif? = True]
  [
    if affichage = 1[
      set pcolor blue + 4 * (effort) - 4]
    if affichage = 2[
      set pcolor agent_type_color]
    
  ]
  
end
@#$#@#$#@
GRAPHICS-WINDOW
589
37
918
284
15
10
10.3
1
10
1
1
1
0
1
1
1
-15
15
-10
10
0
0
1
ticks
30.0

BUTTON
81
231
153
264
NIL
setup_i
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
34
181
206
214
config_i
config_i
1
10
7
1
1
NIL
HORIZONTAL

SLIDER
708
369
880
402
affichage
affichage
1
2
2
1
1
NIL
HORIZONTAL

BUTTON
1051
420
1162
453
NIL
setup_random
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
26
439
89
472
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
137
441
200
474
NIL
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

TEXTBOX
10
16
342
170
config_i:\n1:a population consisting only of rational agents\n2:a population consisting of only shrinking effort\n3: 1 fixed effort else replicator\n4: 1 rational else replicator\n5: 1 high effort else replicator\n6:pas d'equilibr: 40 high effortle reste des agents effort comparator\n7: 40 null effort 40 high effort reste replicator\n8:\n9:\n10: winer imitator population
11
0.0
1

BUTTON
741
417
846
450
NIL
refresh_color
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

PLOT
942
32
1286
290
Effort Moyen
Time
effort
0.0
10.0
0.0
2.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true ";ifelse plot-color = 0\n;[\n;set plot-color 5\n;]\n;[set plot-color plot-color + 10\n;]" "let somme 0\n;set-plot-pen-color plot-color\nask patches with [actif? = True]\n[\n set somme somme + effort\n]\nlet compteur count patches with [actif? = True]\n;plot somme / compteur\nifelse time > 0 \n[\nplot-pen-down\n]\n[plot-pen-up]\nplotxy time somme / compteur"
"pen-1" 1.0 0 -7500403 true "" ""
"pen-2" 1.0 0 -2674135 true "" ""
"pen-3" 1.0 0 -955883 true "" ""

SLIDER
35
313
207
346
Noise
Noise
0
0.5
0
0.01
1
NIL
HORIZONTAL

SLIDER
352
179
524
212
config_%
config_%
1
9
4
1
1
NIL
HORIZONTAL

SLIDER
353
227
525
260
%_high_effort
%_high_effort
0
100
5.6
0.1
1
NIL
HORIZONTAL

BUTTON
372
279
510
312
NIL
setup_%higheffort
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
74
382
168
415
NIL
add_leader
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

TEXTBOX
351
17
501
157
config_%:\n1:Null Effort\n2:Shrinking Effort\n3:Replicator\n4:Profit Comparator\n5:Winner Imitator\n6:Effort Comparator\n7:Averager\n8:Rational\n9:Average Rational
11
0.0
1

TEXTBOX
742
329
892
357
1:effort des agents\n2:type des agents
11
0.0
1

SLIDER
361
368
533
401
effort_initial
effort_initial
0.0001
2
0.9001
0.1
1
NIL
HORIZONTAL

SLIDER
357
439
529
472
effort_agent_fixe
effort_agent_fixe
0
2
1
0.1
1
NIL
HORIZONTAL

TEXTBOX
1036
319
1252
395
setup_random: \n50 High effort et 400 shrinking effort\nPermet de visualisé le déplacement des agents qui n'est pas utilisé dans les configurations étudié.
11
0.0
1

@#$#@#$#@
## WHAT IS IT?

(a general understanding of what the model is trying to show or explain)

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT

(how to use the model, including a description of each of the items in the Interface tab)

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(a reference to the model's URL on the web if it has one, as well as any other necessary credits, citations, and links)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270

@#$#@#$#@
NetLogo 5.1.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

@#$#@#$#@
0
@#$#@#$#@
